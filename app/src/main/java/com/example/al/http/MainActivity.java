package com.example.al.http;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				new AsyncRequest().execute();
			}
		});
	}

	class AsyncRequest extends AsyncTask {
		StringBuilder sb = new StringBuilder();

		@Override
		protected Object doInBackground(Object[] objects) {
			HttpURLConnection urlConnection = null;

			try {
//				URL url = new URL("http://hymn-server-dev.eu.ngrok.io.ngrok.io/media_pages/main");
				URL url = new URL("https://avalonstudio.ru");
				urlConnection = (HttpURLConnection)url.openConnection();
				urlConnection.setRequestMethod("GET");
//				urlConnection.setRequestProperty("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzY29wZSI6Im1vYmlsZV9hcGkiLCJkZXZpY2VfaWQiOiJfZGV2aWNlIiwiYWNjZXNzX3Rva2VuIjoiSW9KazA1UWxvZ0RNVU5xZFFjaVNYalJPRndidWRLbEtuRmxWZnd0Y3QyR2lZb1pTODZHY0VoQVo3U2I5TjlDRSJ9.1qw9Lctw3nTqaqA7tf1k6hPYc9p_oSDrtYbJyHzNxm8" );
				InputStream is = urlConnection.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				int data;
				while ((data = isr.read()) != -1) {
					sb.append((char)data);
				}
				Log.d("__HTTP__", sb.toString());
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (urlConnection != null) {
					urlConnection.disconnect();
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Object o) {
            ((EditText)findViewById(R.id.editText)).setText(sb.toString());
		}
	}
}
